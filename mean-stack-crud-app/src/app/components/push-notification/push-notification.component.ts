import { PushNotificationsService } from "./../../service/push-notifications/push-notifications.service";
import { Component, OnInit, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "app-push-notification",
  templateUrl: "./push-notification.component.html",
  styleUrls: ["./push-notification.component.css"]
})
export class PushNotificationComponent implements OnInit {
  public title: string = "Browser Push Notifications";

  constructor(private _notificationsService: PushNotificationsService) {
    this._notificationsService.requestPermission();
  }

  ngOnInit() {}

  notify() {
    let data: Array<any> = [];
    data.push({
      title: "Approval",
      alertContent: "This is First Alert -- By Debasis Saha"
    });

    data.push({
      title: "Leave Application",
      alertContent: "This is Third Alert -- By Debasis Saha"
    });

    data.push({
      title: "Approval",
      alertContent: "This is Fourth Alert -- By Debasis Saha"
    });
    data.push({
      title: "To Do Task",
      alertContent: "This is Fifth Alert -- By Debasis Saha"
    });

    console.log(data);

    this._notificationsService.generateNotification(data);
  }
}
